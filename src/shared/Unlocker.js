import Request from './Request'
/**
 * Unlocks a module, given a Canvas Course ID, array of students, a module id, and a token.
 * It should be noted this doesn't check more than the first 100 module items. It has the ability
 * to handle it with the pagination done in Request, however it assumes you don't.
 */
export default function unlocker(course_id, student_ids, module_ids, token) {
  student_ids.forEach((student_id) => {
    module_ids.forEach((module_id) => {
      //For each student request first 100 module items, in the module. Only grab items they can see.
      Request("https://luoa.instructure.com/api/v1/courses/" + course_id + "/modules/" + module_id + "/items", true, 1, 100, token, function(resp) {
        JSON.parse(resp).forEach((module) => {
          //For each of the module items they can see.
          if(module.completion_requirement.type == 'must_contribute') {
            //If it's a contribute an empty POST/PUT completes this requirement. Without creating
            // an item that they can then edit, etc.
            if(module.type == "Page") {
              var xhr = new XMLHttpRequest()
              xhr.open("PUT", module.url + "?as_user_id=" + student_id, false)
              xhr.setRequestHeader("Authorization", "Bearer " + token)
              xhr.send()
            }else {
              var xhr = new XMLHttpRequest()
              xhr.open("POST", module.url, false)
              xhr.setRequestHeader("Authorization", "Bearer " + token)
              xhr.send()
            }
          }else if(module.completion_requirement.type == 'must_mark_done') {
            //If an item is must mark as done, we can use the DONE endpoint.
            var xhr = new XMLHttpRequest()
            xhr.open("PUT", module.html_url + '/done?as_user_id=' + student_id, false)
            xhr.setRequestHeader("Authorization", "Bearer " + token)
            xhr.send()
          }else if(module.completion_requirement.type == 'must_view') {
            //Must view use the mark read endpoint.
            var xhr = new XMLHttpRequest()
            xhr.open("POST", module.html_url + '/mark_read?as_user_id=' + student_id, false)
            xhr.setRequestHeader("Authorization", "Bearer " + token)
            xhr.send()
          }else {
            //Otherwise assume it's a score based requirement, or submit requirement, and mark the item as excused.
            var xhr = new XMLHttpRequest()
            xhr.open("PUT", module.url + '/submissions/' + student_id, false)
            var data = new FormData()
            data.append('submission[excuse]', true)
            xhr.setRequestHeader("Authorization", "Bearer " + token)
            xhr.send(data)
          }
        })
      })
    })
  })
}
