import React from 'react'
import Request from '../Request'
import ReactRenderUnknown from './ReactRenderUnknown'
import FontAwesome from 'react-fontawesome'

var StudentList = React.createClass({displayName: 'StudentList',
  getInitialState: function() {
    return {
      student_data: [],
      student_names: [],
      search_term: '',
      search_results: [],
      search_results_id: [],
      page_num: 1
    }
  },
  propTypes: {
    state_callback: React.PropTypes.func.isRequired,
    course_id: React.PropTypes.string.isRequired
  },
  handleNextPage: function(event) {
    this.props.state_callback(this.state.student_data)
  },
  handleBackClick: function(event) {
    this.props.state_callback(null)
  },
  handleStudentClick: function(event) {
    if(event.target.id != -1 || event.target.id != "-1") {
      var to_append = this.state.student_data
      to_append.push(event.target.id)
      var to_append_two = this.state.student_names
      to_append_two.push(event.target.name)
      this.setState({
        student_data: to_append,
        studnet_names: to_append_two
      })
    }
  },
  handleReStudentClick: function(event) {
    var index = event.target.id
    if(index != -1 || index != "-1") {
      var copy = this.state.student_names
      var copy_2 = this.state.student_data
      copy.splice(index, 1)
      copy_2.splice(index, 1)
      this.setState({
        student_names: copy,
        student_data: copy_2
      })
    }
  },
  requestData: function(search) {
    Request("https://luoa.instructure.com/api/v1/courses/" + this.props.course_id + "/users?search_term=" + search, false, null, null, this.props.token_store.grabToken(), (results) => {
      var pRes = JSON.parse(results)
      var toSetNames = []
      var toSetIDs = []
      pRes.forEach((stu) => {
        toSetNames.push(stu.name)
        toSetIDs.push(stu.id)
      })
      this.setState({
        search_results: toSetNames,
        search_results_id: toSetIDs
      })
    })
  },
  handleSearchChange: function(event) {
    var search_string = event.target.value
    this.setState({
      search_results: [],
      search_term: search_string
    })
    if(search_string.length > 2) {
      this.requestData(search_string)
    }
  },
  render: function() {
    return (
      <div id="wrapper">
        <div id='student-list'>
          <input value={this.state.search_term} name="student_search" placeholder="Search for students…" id="student_search" type="text" onChange={this.handleSearchChange} />

          <h1>Results</h1>
          <div className="flex">
            {
              this.state.search_term ? (
                this.state.search_results.map(function(data, i) {
                  return (
                    <label className="student_name" for={data}>
                      <input type="checkbox" id={String(i)} name={data} onChange={this.handleStudentClick}>
                        {data}
                      </input>
                    </label>
                  )
                }, this)
              ) : (
                <ReactRenderUnknown id={"-1"} className={"empty_result"} message={"Search for Some Students...."} />
              )
            }
          </div>
          <hr />
          <h1>Selected Students</h1>
          <div class="flex">
            {
              this.state.student_names.length > 0 ? (
                this.state.student_names.map(function(data, i) {
                  return (
                    <label className="student_name" for={data}>
                      <input type="checkbox" checked="checked" id={String(i)} name={data} onChange={this.handleReStudentClick}>
                        {data}
                      </input>
                    </label>
                  )
                }, this)
              ) : (
                <ReactRenderUnknown className={"empty_result"} id={"-1"} message={"No Students Selected (Yet)."} />
              )
            }
          </div>
          <div className="nav">
            <button id='back_to_course' onClick={this.handleBackClick}>
              <FontAwesome name="arrow-left" />
              Go Back to Course Chooser
            </button>
            <button onClick={this.handleNextPage} id='choose_modules'>
              <FontAwesome name="arrow-right" />
              Choose Modules
            </button>
          </div>
        </div>
      </div>
    )
  }
})

module.exports = StudentList
