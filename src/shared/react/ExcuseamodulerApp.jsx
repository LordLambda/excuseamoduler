import React from 'react'
import TokenStore from '../TokenStore'
import Loading from './Loading'
import SetupComponent from './SetupComponent'
import Request from '../Request'
import StudentList from './StudentList'
import ModuleList from './ModuleList'
import Unlocker from '../Unlocker'

var ExcuseamodulerApp = React.createClass({displayName: 'ExcuseamodulerApp',
  getInitialState: function() {
    return {
      current_state: 'initial',
      course_selected: false,
      students_selected: false,
      module_initial_data: false,
      loading_reason: '',
      token_store: new TokenStore({})
    }
  },
  stateCallback: function(new_state) {
    if(this.state.current_state == 'initial') {
      if(new_state) {
        this.setState({
          current_state: 'students-view',
          course_selected: new_state
        })
      }
    }else if(this.state.current_state == 'students-view') {
      if(new_state) {
        this.setState({
          current_state: 'loading',
          students_selected: new_state,
          loading_reason: 'We are loading Modules for these Student(s)...'
        })
        Request(("https://luoa.instructure.com/api/v1/courses/" + this.state.course_selected + '/modules'), true, 1, 100, this.state.token_store.grabToken(), (modules) => {
          this.setState({
            module_initial_data: JSON.parse(modules),
            current_state: 'modules-view'
          })
        })
      }else {
        this.setState({
          current_state: 'initial'
        })
      }
    }else if(this.state.current_state == 'modules-view') {
      if(new_state) {
        Unlocker(this.state.course_selected, this.state.students_selected, new_state, this.state.token_store.grabToken())
      }
      this.setState({
        current_state: 'students-view'
      })
    }
  },
  render: function() {
    if(this.state.current_state == 'initial') {
      return (
        <SetupComponent token_store={this.state.token_store} state_callback={this.stateCallback} />
      )
    }else if(this.state.current_state == 'loading') {
      return (
        <Loading loading_what={this.state.loading_reason} />
      )
    }else if(this.state.current_state == 'students-view') {
      return (
        <StudentList state_callback={this.stateCallback} course_id={this.state.course_selected} token_store={this.state.token_store} />
      )
    }else if(this.state.current_state == 'modules-view') {
      return (
        <ModuleList module_data={this.state.module_initial_data} state_callback={this.stateCallback} course_id={this.state.course_selected} tokenStore={this.state.tokenStore} />
      )
    }
  }
})

module.exports = ExcuseamodulerApp
