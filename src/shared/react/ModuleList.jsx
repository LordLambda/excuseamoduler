import React from 'react'
import ReactPaginate from 'react-paginate'
import Request from '../Request'
import FontAwesome from 'react-fontawesome'

var ModuleList = React.createClass({displayName: 'ModuleList',
  getInitialState: function() {
    return {
      module_data: this.props.module_data,
      page_num: 1,
      modules_selected: []
    }
  },
  propTypes: {
    state_callback: React.PropTypes.func.isRequired,
    course_id: React.PropTypes.string.isRequired
  },
  handleModuleClick: function(event) {
    if(this.state.modules_selected.length != 0) {
      this.props.state_callback(this.state.modules_selected)
    }
  },
  requestData: function(page) {
    Request(("https://luoa.instructure.com/api/v1/courses/" + this.props.course_id + "/modules"), true, page, 100, this.props.token_store.grabToken(), function callback(modules) {
      this.setState({module_data: modules})
    })
  },
  handlePageClick: function(data) {
    this.requestData(data.selected)
    this.setState({page_num: data.selected})
  },
  handleBackClick: function(event) {
    this.handleModuleClick(null)
  },
  filterModules: function(data) {
      //Create an optional filter function in case you want to filter
      //For Example:
      /*
      data.forEach((potentialModule) => {
        if(potentialModule.name.indexOf("skippable") > -1) {
          ///The modules name contains "skippable"
          retV.push(potentialModule)
        }
      })
      */
      //That would only show modules with "skippable" in the name.
      //It should be noted that this is only client side filtering not server side.
      //So if you have 100 modules it fetches, but only one of the 99 has skippable in the name
      //Although you fetched all 100 the user will only see one, which could lead to confusion.
      var retV = []
      if(data) {
        data.forEach((potentialModule) => {
          retV.push(potentialModule)
        })
      }
      return retV
  },
  handleClicked: function(event) {
    var modules_selected_new = this.state.modules_selected
    if(modules_selected_new.indexOf(event.target.id) == -1) {
      modules_selected_new.push(event.target.id)
    }else {
      modules_selected_new.splice(modules_selected_new.indexOf(event.target.id), 1)
    }
    this.setState({
      modules_selected: modules_selected_new
    })
  },
  render: function() {
    var filteredModules = this.filterModules(this.state.module_data)
    return (
      <div id="wrapper">
        <div id='module_list'>
          <h1>Modules</h1>

          <div className="module_list flex">
            {
              filteredModules.map(function(data, i) {
                if(this.state.modules_selected.indexOf(String(data.id)) == -1) {
                  return (
                    <label className="module_name" for={data.name}>
                      <input type={"checkbox"} name={data.name} id={data.id} onChange={this.handleClicked}>
                        {data.name}
                      </input>
                    </label>
                  )
                }
              }, this)
            }
          </div>

          <ReactPaginate previousLabel={"Previous"}
                        nextLabel={"Next"}
                        breakLabel={<li className="break"><a href="">...</a></li>}
                        pageNum={this.state.page_num}
                        marginPagesDisplayed={1}
                        pageRangeDisplayed={3}
                        clickCallback={this.handlePageClick}
                        containerClassName={"paginated-modules"}
                        subContinerClassName={"paginated-module-pages"}
                        activeClassName={"active"} />
          <br />
          <hr />

          <h1>Selected Module(s)</h1>

          <div class="flex">
            {
              this.state.modules_selected.length > 0 ? (
                filteredModules.map(function(data, i) {
                  if(this.state.modules_selected.indexOf(String(data.id)) > -1) {
                    return (
                      <label className="module_name" for={data.name}>
                        <input type={"checkbox"} checked={"checked"} name={data.name} id={data.id} onChange={this.handleClicked}>
                          {data.name}
                        </input>
                      </label>
                    )
                  }
                }, this)
              ) : (
                <div class="empty_result">
                  No Modules Selected.
                </div>
              )
            }
          </div>

          <div class="nav">
            <button id="back_to_student" onClick={this.handleBackClick}>
              <FontAwesome name="arrow-left" />
              Back
            </button>
            <button id="unlock_module" onClick={this.handleModuleClick}>
              Unlock Module(s)
              <FontAwesome name="unlock" />
            </button>
          </div>
        </div>
      </div>
    )
  }
})

module.exports = ModuleList
