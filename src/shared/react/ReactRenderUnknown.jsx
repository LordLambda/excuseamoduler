import React from 'react'

var ReactRenderUnknown = React.createClass({displayName: 'ReactRenderUnknown',
  getInitialState: function () {
    return {
      message: this.props.message
    }
  },
  propTypes: {
    message: React.PropTypes.string,
    on_click: React.PropTypes.func,
    id: React.PropTypes.string
  },
  render: function() {
    if(!this.props.button) {
      return (
        <div className={"react-unknown"} onClick={this.props.onClick ? this.props.onClick : function(event){}} id={this.props.id ? this.props.id : ''}>
          {
            this.state.message
          }
        </div>
      )
    }else {
      return (
        <button className={"react-unknown"} onClick={this.props.on_click ? this.props.on_click : function(event) {}} id={this.props.id ? this.props.id : ''}>
          {
            this.state.message
          }
        </button>
      )
    }
  }
})

module.exports = ReactRenderUnknown
