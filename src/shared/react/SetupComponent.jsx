import React from 'react'
import isaac from 'isaac'

var SetupComponent = React.createClass({displayName: 'SetupScreen',
  getInitialState: function() {
    return {
      token: '',
      course_id: '',
      shouldDisable: true
    }
  },
  handleClick: function(event) {
    event.preventDefault()
    if(this.props.token_store) {
      isaac.seed(this.state.token)
      this.props.token_store.setPassword(String(isaac.random()))
      this.props.token_store.setToken(this.state.token)
    }
    this.props.state_callback(this.state.course_id)
  },
  handleInput: function(event) {
    var value = event.target.value
    if(event.target.id == "token_token") {
      if((this.state.course_id) != '') {
        this.setState({
          shouldDisable: false,
          token: value
        })
      }else {
        this.setState({
          token: value
        })
      }
    }else {
      if(this.state.token) {
        this.setState({
          shouldDisable: false,
          course_id: value
        })
      }else {
        this.setState({
          course_id: value
        })
      }
    }
  },
  render: function() {
    return (
      <div id="wrapper">
        <form id="pass_form">
          <fieldset id="pass_field_set" className="container">
            <label htmlFor="token_token">API Token: </label>
            <div class="passwordField" data-valid="true" data-score="0" data-entropy="0">
              <input class="passwordField__input" type="text" value={this.state.token} id="token_token" name="token_token" placeholder="Enter your API token for Canvas!" required="" pattern="^\d{1,}~[a-zA-Z0-9]{64}$" title="[numbers]~[64 alphanumeric characters]" onChange={this.handleInput} /> <span class="help">Make sure to enter the entire token.</span>
            </div>
          </fieldset>
          <fieldset id="choose_course" class="container">
              <label for="course_name">Course ID</label> <input name="course_name" value={String(this.state.course_id)} placeholder="Enter the Canvas Course ID" id="course_name" required="" pattern="[0-9]{1,15}" type="text" title="Canvas ID, not the SIS ID" onChange={this.handleInput} /> <span class="help">Numbers only.</span>
          </fieldset>
          {
            this.state.shouldDisable ? (
              <button id="pass_button" disabled={"disabled"}>
                Submit
              </button>
            ) : (
              <button id="pass_button" onClick={this.handleClick}>
                Submit
              </button>
            )
          }
        </form>
      </div>
    )
  }
})

module.exports = SetupComponent
