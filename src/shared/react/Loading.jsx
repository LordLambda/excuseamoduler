import React from 'react'
import ReactRenderUnknown from './ReactRenderUnknown'

var Loading = React.createClass({displayName:'LoadingScreen',
  render: function() {
    return (
      <div id="wrapper">
        <ReactRenderUnknown id={"loading_box"} message={this.props.loading_what} />
      </div>
    )
  }
})

module.exports = Loading
