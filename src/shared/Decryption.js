import node_cryptojs from 'node-cryptojs-aes'
/*
 * The Decryption routine. This was originally done with Triple-Sec (SCrypt, Twofist, AES, etc.)
 * However chrome barfed on that sometimes taking way to long to decrypt. As such we've gone with
 * AES, still way above industry compliancy however, much faster in the browser. I've made the
 * Encryption/Decryption routines seperated so you can swap them out, if you'd prefer.
 */
export default function(key, enc_data) {
  var realKey = key.toString('base64')
  var CryptoJS = node_cryptojs.CryptoJS
  var JsonFormatter = node_cryptojs.JsonFormatter
  var decrypted = CryptoJS.AES.decrypt(enc_data, realKey, { format: JsonFormatter })
  return CryptoJS.enc.Utf8.stringify(decrypted)
}
