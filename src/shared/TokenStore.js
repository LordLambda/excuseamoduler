import encrypt from './Encryption'
import decrypt from './Decryption'
/**
 * A token store to keep the token encrypted in memory.
 */
var token = class TokenStore {
  /**
   * The constructor for the class.
   */
  constructor() {
    this.token = ''
  }
  /**
   * Set the password, and re-encrypt the token if need be.
   */
  setPassword(password) {
    if(this.tokenz) {
      this.tokenzz = ''
      if(!this.password) {
        this.tokenzz = this.tokenz
      }else {
        this.tokenzz = decrypt(this.password, this.tokenz)
      }
      this.tokenz = ''
      this.password = (password)
      if(this.tokenzz != '') {
        this.token = encrypt(this.password, this.tokenzz)
        this.tokenzz = ''
      }
    }else {
      this.password = (password)
    }
  }
  /**
   * Grab the token.
   */
  grabToken() {
    if(this.password) {
      return decrypt(this.password, this.tokenz)
    }else {
      return this.tokenz
    }
  }
  /**
   * Set the token.
   */
  setToken(tokenzzz) {
    if(this.password) {
      this.tokenz = encrypt(this.password, tokenzzz)
    }else {
      this.tokenz = tokenzzz
    }
  }
}

module.exports = token
