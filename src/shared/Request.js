/**
 * A Default request function. This is one of two places you'll want to replace in case of
 * you wanting to add in CORS support so you don't have to run chrome in unsafe mode.
 * The requeset function here is what makes all the calls to GET items, list of students,
 * modules, etc. All proxy through this request class. So again if you want to change it,
 * you only have to change it in one placce instead of going all over the place.
 */
export default function(base_url, use_pages, page_num, per_page, token, callback) {
  var formatted_url = ''
  if(use_pages) {
    formatted_url = (base_url.indexOf("?") > -1 ? base_url + '&per_page=' + per_page + '&page=' + page_num : base_url + '?per_page=' + per_page + '&page=' + page_num)
  }else {
    formatted_url = base_url
  }
  var xhr = new XMLHttpRequest()
  xhr.open("GET", formatted_url, true)
  xhr.setRequestHeader("Authorization", "Bearer " + token)
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
      callback(xhr.responseText)
    }
  }
  xhr.send()
  return
}
