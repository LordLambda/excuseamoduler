import React from 'react'
import ReactDOM from 'react-dom'
import ExcuseamodulerApp from 'shared/react/ExcuseamodulerApp'
import './Excusamodular.css'

ReactDOM.render(<ExcuseamodulerApp />, document.getElementById("wrapper"))
